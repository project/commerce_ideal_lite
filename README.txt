CONTENTS OF THIS FILE:
-----------------------

  * Description
  * Installation

Description
----------

Provides the iDEAL lite payment to commerce.

Installation
------------

 * Copy the whole commerce_ideal_lite directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate the 
	 Commerce iDEAL Lite module.
 * Navigate to the iDEAL Lite payment methode rule at 
   admin/commerce/config/payment-methods and edit the "iDEAL Lite" rule.
 * Edit the action to enter your merchant information.
